#ifndef _YASM_LIBYASM_STDINT_H
#define _YASM_LIBYASM_STDINT_H 1
#ifndef _GENERATED_STDINT_H
#define _GENERATED_STDINT_H "yasm 1.3.0"
/* generated using /Volumes/Android/buildbot/src/android/ndk-release-r23/prebuilts/clang/host/darwin-x86/clang-r416183c1/bin/clang --target=x86_64-apple-darwin -B/Volumes/Android/buildbot/src/android/ndk-release-r23/prebuilts/gcc/darwin-x86/host/i686-apple-darwin-4.2.1/x86_64-apple-darwin11/bin -mmacosx-version-min=10.9 -DMACOSX_DEPLOYMENT_TARGET=10.9 -isysroot/Applications/Xcode9.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.13.sdk -Wl,-syslibroot,/Applications/Xcode9.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.13.sdk -mlinker-version=305 -Os -fomit-frame-pointer -w -s */
#define _STDINT_HAVE_STDINT_H 1
#include <stdint.h>
#endif
#endif
